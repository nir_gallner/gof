package co.verisoft.patterns.observer;

import java.text.DecimalFormat;



public class GetTheStock implements Runnable{

	private int startTime;
	private String stock;
	private double price;
	
	private Subject stockGrabber;
	
	public GetTheStock(Subject stockGrabber, int newStartTime, String newStock, double newPrice) {
		this.startTime = newStartTime;
		this.stock = newStock;
		this.price = newPrice;
		this.stockGrabber = stockGrabber;
				
	}
	@Override
	public void run() {
		for (int i=0 ; i<=20 ; i++) {
			try {
				Thread.sleep(startTime);
			}
			
			catch(InterruptedException e) {
				// No-op
			}
			
			double random = (Math.random() * (.06)) -.03;
			DecimalFormat df  = new DecimalFormat("#.##");
			price = Double.valueOf(df.format((price + random)));
			
			if (stock == "IBM") ((StockGrabber)stockGrabber).setIbmPrice(price);
			
			if (stock == "AAPL") ((StockGrabber)stockGrabber).setaaplPrice(price);
			
			if (stock == "GOOG") ((StockGrabber)stockGrabber).setgoogPrice(price);
			
			System.out.println(stock + ": " + df.format(price + random) + " " + 
					df.format(random));
			
			System.out.println();
			
		}
			
		
	}
	

}

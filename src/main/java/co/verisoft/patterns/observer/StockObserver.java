package co.verisoft.patterns.observer;

public class StockObserver implements Observer
{
    
    private double ibmPrice;
    private double aaplPrice;
    private double googPrice;

    private static int observerIdTracker = 0;
    private int obsreverID;
    private Subject stockGrabber;
    
    public StockObserver(Subject stockGrabber){
        this.stockGrabber = stockGrabber;
        this.obsreverID = ++observerIdTracker;
        System.out.println("New observer " + this.obsreverID);

        stockGrabber.register(this);
    }

    @Override
    public void update(double ibmPrice, double aaplPrice, double googPrice)
    {
        this.ibmPrice = ibmPrice;
        this.aaplPrice = aaplPrice;
        this.googPrice = googPrice;
        System.out.println(this.toString()); 

    }

    @Override
    public String toString()
    {
        String s = this.obsreverID +"\n";
        s += "IBM: " + ibmPrice + "\n";
        s += "AAPL: " + aaplPrice + "\n";
        s += "GOOG: " + googPrice + "\n";
        return s;

    }


}

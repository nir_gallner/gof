package co.verisoft.patterns.observer;

public class GrabStocks {
    public static void main(String[] args)
    {
        StockGrabber stockGrabber = new StockGrabber();
        
        StockObserver stockObserver1 = new StockObserver(stockGrabber);
        stockGrabber.setIbmPrice(197.00);
        stockGrabber.setaaplPrice(677.6);
        stockGrabber.setgoogPrice(676.4);
        
        StockObserver stockObserver2 = new StockObserver(stockGrabber);
        stockGrabber.setIbmPrice(197.00);
        stockGrabber.setaaplPrice(677.6);
        stockGrabber.setgoogPrice(676.4);
        
        stockGrabber.unregister(stockObserver2);
        stockGrabber.setIbmPrice(197.00);
        stockGrabber.setaaplPrice(677.6);
        stockGrabber.setgoogPrice(676.4);
        
        Runnable getIBM = new GetTheStock(stockGrabber, 2, "IBM", 197.0);
        Runnable getAAPL = new GetTheStock(stockGrabber, 2, "AAPL", 677.6);
        Runnable getGOOG = new GetTheStock(stockGrabber, 2, "GOOG", 676.4);
        
        new Thread(getIBM).start();
        new Thread(getAAPL).start();
        new Thread(getGOOG).start();

    }
    
}

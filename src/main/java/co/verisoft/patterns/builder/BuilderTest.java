package co.verisoft.patterns.builder;

import java.util.List;

public class BuilderTest {

	public static void main(String[] args) {
		
		Name name = new Name.Builder().firstName("NIr")
				.middleName("Strerling")
				.lastName("Gallner")
				.build();
		
		Address address = new Address.Builder().city("Zoran")
				.street("Hahadarim")
				.houseNumber(113)
				.zipCode("42823")
				.build();
		
		Account account = new Account.Builder().address(address)
				.email("nir@gmail.com")
				.id(32)
				.name(name)
				.build();
		
		System.out.println(account.toString());
				

	}

}

package co.verisoft.patterns.builder;

import java.util.List;

public class Name {

	private final String firstName;
	private final String middleName;
	private final String lastName;

	private Name(Builder builder) {
		this.firstName = builder.firstName;
		this.middleName = builder.middleName;
		this.lastName = builder.lastName;
	}

	@Override
	public String toString() {
		return firstName + " " + middleName + " " + lastName;
	}
	public static class Builder {
		private String firstName;
		private String middleName;
		private String lastName;

		public Builder firstName(final String firstName) {
			this.firstName = firstName;
			return this;
		}

		public Builder middleName(final String middleName) {
			this.middleName = middleName;
			return this;
		}

		public Builder lastName(final String lastName) {
			this.lastName = lastName;
			return this;
		}

		public Name build() {
			return new Name(this);
		}
	}

	public String getFirstName() {
		return firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public String getLastName() {
		return lastName;
	}

}

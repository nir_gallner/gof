package co.verisoft.patterns.abstractFactory;

import co.verisoft.patterns.factory.EnemyShip;

public abstract class EnemyShipBuilding {

	protected abstract EnemyShip makeEnemyShip(String typeOfShip);
	
	public EnemyShip orderTheShip(String typeOfShip) {
		EnemyShip theEnemyShip = makeEnemyShip(typeOfShip);
		
		theEnemyShip.makeShip();
		theEnemyShip.displayEnemyShip();
		theEnemyShip.followHeroSHip();
		theEnemyShip.enemyShipShoots();
		
		return theEnemyShip;
		
	}
}

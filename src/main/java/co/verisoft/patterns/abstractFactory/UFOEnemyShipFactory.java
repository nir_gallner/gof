package co.verisoft.patterns.abstractFactory;

import co.verisoft.patterns.factory.ESEngine;
import co.verisoft.patterns.factory.ESUFOEngine;
import co.verisoft.patterns.factory.ESUFOGun;
import co.verisoft.patterns.factory.ESWeapon;

public class UFOEnemyShipFactory implements EnemyShipFactory{

	@Override
	public ESWeapon addESGun() {
		return new ESUFOGun();
	}

	@Override
	public ESEngine addESEngine() {
		return new ESUFOEngine();
	}

}

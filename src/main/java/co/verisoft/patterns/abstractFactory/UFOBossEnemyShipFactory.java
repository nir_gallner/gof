package co.verisoft.patterns.abstractFactory;

import co.verisoft.patterns.factory.ESBossUFOEngine;
import co.verisoft.patterns.factory.ESBossUFOGun;
import co.verisoft.patterns.factory.ESEngine;
import co.verisoft.patterns.factory.ESWeapon;

public class UFOBossEnemyShipFactory implements EnemyShipFactory{

	@Override
	public ESWeapon addESGun() {
		return new ESBossUFOGun();
	}

	@Override
	public ESEngine addESEngine() {
		return new ESBossUFOEngine();
	}

}
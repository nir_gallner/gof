package co.verisoft.patterns.abstractFactory;

import co.verisoft.patterns.factory.ESEngine;
import co.verisoft.patterns.factory.ESWeapon;

public interface EnemyShipFactory {

	public ESWeapon addESGun();
	public ESEngine addESEngine();
}

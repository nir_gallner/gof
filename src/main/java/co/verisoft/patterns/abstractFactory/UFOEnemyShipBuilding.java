package co.verisoft.patterns.abstractFactory;

import co.verisoft.patterns.factory.EnemyShip;
import co.verisoft.patterns.factory.UFOEnemyShip;

public class UFOEnemyShipBuilding extends EnemyShipBuilding{

	@Override
	protected EnemyShip makeEnemyShip(String typeOfShip) {
		EnemyShip theEnemyShip = null;
		
		if (typeOfShip.equals("UFO")) {
			EnemyShipFactory shipPartsFactory = new UFOEnemyShipFactory();
			theEnemyShip = new UFOEnemyShip(shipPartsFactory);
			theEnemyShip.setName("UFO Grunt shipt");
		}
		
		if (typeOfShip.equals("UFO BOSS")) {
			EnemyShipFactory shipPartsFactory = new UFOBossEnemyShipFactory();
			theEnemyShip = new UFOBossEnemyShip(shipPartsFactory);
			theEnemyShip.setName("UFO Boss shipt");
		}
		
		return theEnemyShip;
	}

}

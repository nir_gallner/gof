package co.verisoft.patterns.abstractFactory;

import co.verisoft.patterns.factory.EnemyShip;

public class AbstractFactoryTest {

	public static void main(String[] args) {
		
		EnemyShipBuilding makeUFOs = new UFOEnemyShipBuilding() ;
		
		EnemyShip theGrunt = makeUFOs.orderTheShip("UFO");
		System.out.println(theGrunt +  "\n");
		
		EnemyShip theBoss = makeUFOs.orderTheShip("UFO BOSS");
		System.out.println(theBoss +  "\n");
		

	}

}

package co.verisoft.patterns.factory;


import co.verisoft.patterns.abstractFactory.EnemyShipFactory;
import co.verisoft.patterns.factory.EnemyShip;

public class BigUFOEnemyShip extends EnemyShip {

	EnemyShipFactory shipFactory;
	
	
	public BigUFOEnemyShip(EnemyShipFactory shipFactory) {
		this.shipFactory = shipFactory;
		
	}
	
	public void makeShip() {
		System.out.println("Making enemy ship " + getName());
		
		weapon = shipFactory.addESGun();
		engine = shipFactory.addESEngine();
		
	
	}
}

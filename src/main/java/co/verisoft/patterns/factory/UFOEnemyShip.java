package co.verisoft.patterns.factory;

import co.verisoft.patterns.abstractFactory.EnemyShipFactory;

public class UFOEnemyShip extends EnemyShip {

	EnemyShipFactory shipFactory;
	
	
	public UFOEnemyShip(EnemyShipFactory shipFactory) {
		this.shipFactory = shipFactory;
		
	}
	
	public void makeShip() {
		System.out.println("Making enemy ship " + getName());
		
		weapon = shipFactory.addESGun();
		engine = shipFactory.addESEngine();
		
	
	}
}

package co.verisoft.patterns.factory;

public abstract class EnemyShip {

	private String name;
	protected ESWeapon weapon;
	protected ESEngine engine;
	
	public abstract void makeShip();
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		String infoShip = "The " + name + " has a top speed of " +
				engine + " and an attack power of " + weapon;
		
		return infoShip;
	}
	
	
	public void followHeroSHip() {
		System.out.println(getName() + " is following the hero");
	}
	
	public void displayEnemyShip() {
		System.out.println(getName() +" is on screen");
	}

	public void enemyShipShoots() {
		// TODO Auto-generated method stub
		
	}
	

}

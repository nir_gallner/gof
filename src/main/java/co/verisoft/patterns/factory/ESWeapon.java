package co.verisoft.patterns.factory;

public interface ESWeapon {
	
	public String toString();
}

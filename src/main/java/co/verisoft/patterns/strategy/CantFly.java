
package co.verisoft.patterns.strategy;

public class CantFly implements Flys {
    
    @Override
    public String fly() {
        return "I'm not flying";
    }

}

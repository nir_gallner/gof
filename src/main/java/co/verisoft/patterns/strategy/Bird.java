package co.verisoft.patterns.strategy;

public class Bird extends Animal{

    public Bird(){
        super();
        flyingType = new ItFlys();
        setSound("twitty");
    }
}

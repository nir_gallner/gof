
package co.verisoft.patterns.strategy;

public class ItFlys implements Flys {
    
    @Override
    public String fly() {
        return "I'm flying";
    }

}

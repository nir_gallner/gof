package co.verisoft.pattern.prototype;

public interface Animal extends Cloneable {
	public Animal makeCopy();
}

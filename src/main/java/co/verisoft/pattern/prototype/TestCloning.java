package co.verisoft.pattern.prototype;

public class TestCloning {

	public static void main(String[] args) {
		CloneFactory animalMaker = new CloneFactory();
		
		Sheep sally = new Sheep();
		Sheep clonedSheep = (Sheep)animalMaker.getClone(sally);
		
		System.out.println(clonedSheep.toString());
		
	}
}

package test;

public class Test{
    private String username;
    private String password;
    
    public String login() {
	return "log in with " + this.username + "  and "+ this.password;
    }

    public String getUsername() {
        return username;
    }

    public Test setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Test setPassword(String password) {
        this.password = password;
        return this;
    }
    
    public static void main(String[] args) {
	Test test = new Test();
	String result = test
	.setPassword("password")
	.setUsername("username")
	.login();
	System.out.println(result);
    }
    
    

}